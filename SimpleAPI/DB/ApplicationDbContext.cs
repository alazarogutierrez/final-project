﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using SimpleAPI.Entities;

namespace SimpleAPI.DB
{
    public class ApplicationDbContext : DbContext 
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) :
             base(options){}

        public DbSet<Person> Persons{ get; set; }
    }
}
