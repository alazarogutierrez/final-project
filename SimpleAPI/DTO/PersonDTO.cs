﻿namespace SimpleAPI.DTO
{
    public class PersonDTO
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Role { get; set; }
    }
}
