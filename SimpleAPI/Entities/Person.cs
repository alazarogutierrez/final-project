﻿using System.ComponentModel.DataAnnotations.Schema;

namespace SimpleAPI.Entities
{
    [Table("Person")]
    public class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Role { get; set; }
    }
}
