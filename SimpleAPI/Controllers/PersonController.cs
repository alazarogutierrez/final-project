﻿using Microsoft.AspNetCore.Mvc;
using SimpleAPI.DB;
using SimpleAPI.DTO;
using SimpleAPI.Entities;
using System.Linq;

namespace SimpleAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PersonController : ControllerBase
    {
        private readonly ApplicationDbContext _db;
        public PersonController(ApplicationDbContext db)
        {
            _db = db;
        }

        [HttpGet]
        public ActionResult Get()
        {
            return Ok(_db.Persons.ToList());
        }

        [HttpPost]
        public ActionResult Save(PersonDTO person)
        {
            var personCreated = new Person
            {
                Name = person.Name,
                LastName = person.LastName,
                Role = person.Role,
            };

            _db.Persons.Add(personCreated);
            _db.SaveChanges();

            return Ok(personCreated);
        }
    }
}
