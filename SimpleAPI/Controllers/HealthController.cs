﻿using Microsoft.AspNetCore.Mvc;
using System;

namespace SimpleAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HealthController : ControllerBase
    {
        [HttpGet]
        public IActionResult Get()
        {
            return Ok($"This app works and points to {Environment.GetEnvironmentVariable("DBHOST")}");
        }
    }
}
